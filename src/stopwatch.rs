use chrono::{Duration, NaiveDateTime, Utc};

/// Simple stopwatch.
pub struct Stopwatch {
    start: NaiveDateTime,
    state: SWState,
}

pub struct SWTime {
    pub hours: u16,
    pub mins: u8,
    pub secs: u8,
    pub msecs: u8,
}

pub enum SWState {
    Stopped,
    Running,
    Paused,
}

impl Stopwatch {
    /// Creates a new stopwatch and initialises the start value with zero.
    pub fn new() -> Self {
        Stopwatch {
            start: NaiveDateTime::from_timestamp(0, 0),
            state: SWState::Running,
        }
    }

    /// Sets the start time to the current one.
    pub fn start(&mut self) {
        // Using UTC here might be a problem, since leap years
        // and even leap seconds are a thing
        let start_utc = Utc::now();
        self.start = NaiveDateTime::from_timestamp(
            start_utc.timestamp(),
            start_utc.timestamp_subsec_nanos(),
        );
    }

    /// Gets the duration from stopwatch start to current time.
    pub fn get_duration(&self) -> Duration {
        let current_utc = Utc::now();
        let current = NaiveDateTime::from_timestamp(
            current_utc.timestamp(),
            current_utc.timestamp_subsec_nanos(),
        );
        current.signed_duration_since(self.start)
    }

    /// Gets the amount of passed hours (u16 for the crazy ones)
    pub fn get_timer_hours(&self) -> u16 {
        self.get_duration().num_hours() as u16
    }

    /// Gets the amount of passed minutes in the current hour
    pub fn get_timer_minutes(&self) -> u8 {
        let mut dur_min = self.get_duration().num_minutes();
        dur_min -= self.get_duration().num_hours() * 60;
        dur_min as u8
    }

    /// Gets the amount of passed seconds in the current minute
    pub fn get_timer_seconds(&self) -> u8 {
        let mut dur_sec = self.get_duration().num_seconds();
        dur_sec -= self.get_duration().num_minutes() * 60;
        dur_sec as u8
    }

    /// Gets the first two digits of the amount of passed
    /// milliseconds in the current second
    pub fn get_timer_milliseconds(&self) -> u8 {
        let mut dur_ms = self.get_duration().num_milliseconds();
        dur_ms -= self.get_duration().num_seconds() * 1000;
        dur_ms /= 10;
        dur_ms as u8
    }

    pub fn get_timer(&self) -> SWTime {
        SWTime {
            hours: self.get_timer_hours(),
            mins: self.get_timer_minutes(),
            secs: self.get_timer_seconds(),
            msecs: self.get_timer_milliseconds(),
        }
    }

    /// Gets a formatted Version of the passed time as
    /// (h:)mm:ss.{100th fraction of a second}
    pub fn get_duration_fmt(&self) -> String {
        let mut dur_fmt = String::new();

        let dur_hrs = self.get_timer_hours();
        let dur_mins = self.get_timer_minutes();
        let dur_secs = self.get_timer_seconds();
        let dur_ms = self.get_timer_milliseconds();

        // Don't display hours, if timer hasn't been running for an hour
        match dur_hrs {
            0 => (),
            _ => dur_fmt.push_str(&format!("{}:", dur_hrs)),
        }

        // Push an extra '0' to the display string,
        // so that single digit numbers like '3' get displayed as '03'
        match dur_mins {
            0...9 => dur_fmt.push_str("0"),
            _ => (),
        }
        dur_fmt.push_str(&format!("{}:", dur_mins));

        // Same as above
        match dur_secs {
            0...9 => dur_fmt.push_str("0"),
            _ => (),
        }
        dur_fmt.push_str(&format!("{}.", dur_secs));

        // Same as above
        match dur_ms {
            0...9 => dur_fmt.push_str("0"),
            _ => (),
        }
        dur_fmt.push_str(&format!("{}", dur_ms));

        dur_fmt
    }
}
