use cairo::{Context, Format, ImageSurface};
use gtk;
use gtk::*;
use std::process;
use state::ActiveMetaData;
use std::cell::RefCell;
use std::rc::Rc;
use ui::render::render_time;

pub struct App {
    pub window: Window,
    pub area: Rc<RefCell<DrawingArea>>,
    pub meta_data: Rc<RefCell<ActiveMetaData>>,
    pub meta_old: Rc<RefCell<ActiveMetaData>>
}

pub struct ConnectedApp(App);

impl App {
    pub fn new() -> App {
        if gtk::init().is_err() {
            eprintln!("Application startup failed at initilisation!");
            process::exit(1);
        }

        const WIDTH: i32 = 400;
        const HEIGHT: i32 = 300;

        let window = Window::new(WindowType::Toplevel);

        window.set_title("Pendulum");
        window.set_default_size(WIDTH, HEIGHT);

        window.connect_delete_event(move |_, _| {
            main_quit();
            Inhibit(false)
        });


        // TODO: Also setup max. box height
        let mut box_width = 0f64;
        let mut box_height = 0f64;
        let cr = Context::new(&ImageSurface::create(Format::Rgb24, 500, 500).unwrap());
        cr.set_font_size(90.0);

        for i in 0..59 {
            let display;
            match i {
                0...9 => display = String::from(format!("0{}", i)),
                _ => display = String::from(format!("{}", i)),
            }

            let te = cr.text_extents(&display);
            if te.width > box_width {
                box_width = te.width;
            }

            if te.height > box_height {
                box_height= te.height;
            }
        }

        let area_free = DrawingArea::new();
        window.add(&area_free);
        let area = Rc::new(RefCell::new(area_free));

        let meta_data = Rc::new(RefCell::new(ActiveMetaData::new()));
        let meta_old = Rc::clone(&meta_data);
        meta_data.borrow_mut().stopwatch.start();
        meta_data.borrow_mut().box_width = box_width;
        meta_data.borrow_mut().box_height = box_height;
        println!("{}", box_width);

        App {
            window,
            area,
            meta_data,
            meta_old
        }
    }

    pub fn connect_events(self) -> ConnectedApp {
        let meta_data = self.meta_data.clone();
        let area = self.area.clone();
        let m_old = self.meta_old.clone();
        let mut meta_old = Rc::downgrade(&self.meta_old);

        area.borrow().connect_draw(move |_, cr| {
            cr.set_source_rgb(1., 1., 1.);
            cr.paint();
            Inhibit(false)
        });

        gtk::timeout_add_seconds(1, move || {
            let timer = meta_data.borrow().stopwatch.get_timer();
            let timer_old = m_old.borrow().stopwatch.get_timer();
            let box_width = meta_data.borrow().box_width;
            let box_height = meta_data.borrow().box_height;
            area.borrow().connect_draw(move |_, cr| {
                render_time(&cr, &timer, &timer_old, &box_width, &box_height);
                Inhibit(false)
            });
            area.borrow_mut().queue_draw_area(0, 0, 400, 300);
            let mut meta_old = meta_old.upgrade().unwrap();
            meta_old.clone_from(&meta_data);

            Continue(true)
        });
        ConnectedApp(self)
    }
}

impl ConnectedApp {
    pub fn then_execute(self) {
        self.0.window.show_all();
        gtk::main();
    }
}
