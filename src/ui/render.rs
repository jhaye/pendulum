use stopwatch::SWTime;
use cairo::Context;

pub fn render_time(cr: &Context, timer: &SWTime, timer_old: &SWTime, box_width: &f64, box_height: &f64) {
    cr.set_font_size(90.0);

    let secs_display;
    let mins_display;
    let hours_display: Option<String>;

    match timer.secs {
        0...9 => secs_display = String::from(format!("0{}", timer.secs)),
        _ => secs_display = String::from(format!("{}", timer.secs)),
    }

    match (timer.mins) {
        0...9 => mins_display = String::from(format!("0{}", timer.mins)),
        _ => mins_display = String::from(format!("{}", timer.mins)),
    }

    match timer.hours {
        0 => hours_display = None,
        _ => hours_display = Some(String::from(format!("{}", timer.hours))),
    }

    let tes = cr.text_extents(&secs_display);

    cr.set_source_rgb(1., 1., 1.);
    // the additional 1 accounts for the text escaping the redrawing box
    // might have something to do with anti-aliasing
    cr.rectangle(400. - box_width, 100. - box_height + 1., *box_width, *box_height);
    cr.fill();
    cr.move_to(400. - box_width - tes.x_bearing, 100.);
    cr.set_source_rgb(0., 0., 0.);
    cr.show_text(&secs_display);

    cr.move_to(400. - box_width - (box_width / 3.), 100.);
    cr.show_text(":");

    let tem = cr.text_extents(&mins_display);

    //if timer_old.mins != timer.mins {
        cr.set_source_rgb(1., 1., 1.);
        cr.rectangle(400. - (box_width*2.) - (box_width / 3.), 100. - box_height + 1., *box_width, *box_height);
        cr.fill();
        cr.move_to(400. - (box_width * 2.) - (box_width / 3.) - tem.x_bearing, 100.);
        cr.set_source_rgb(0., 0., 0.);
        cr.show_text(&mins_display);
    //}

    if hours_display.is_some() {
        let hours_display = hours_display.unwrap();
        let teh = cr.text_extents(&hours_display);
        cr.move_to(400. - (box_width*2.) - ((box_width / 3.) * 2.), 100.);
        cr.show_text(":");
        cr.set_source_rgb(1., 1., 1.);
        cr.rectangle(400. - (box_width*3.) - ((box_width / 3.)*2.), 100. - box_height + 1., *box_width, *box_height);
        cr.fill();
        cr.move_to(400. - (box_width * 3.) - ((box_width / 3.)*2.) - teh.x_bearing + (box_width - teh.width), 100.);
        cr.set_source_rgb(0., 0., 0.);
        cr.show_text(&hours_display);

    }
}
