use stopwatch::Stopwatch;

pub struct ActiveMetaData {
    pub stopwatch: Stopwatch,
    pub box_width: f64,
    pub box_height: f64
}

impl ActiveMetaData {
    pub fn new() -> Self {
        let stopwatch = Stopwatch::new();
        ActiveMetaData {
            stopwatch: stopwatch,
            box_width: 0.0,
            box_height: 0.0
        }
    }
}
