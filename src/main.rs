extern crate cairo;
extern crate chrono;
extern crate gtk;

pub mod ui;
pub mod state;
pub mod stopwatch;

use ui::App;

fn main() {
    App::new().connect_events().then_execute();
}
